### Student.com Technical Exercise

# Installation:

  install compass http://compass-style.org/install/

 - npm install
 - npm install -g grunt-cli
 - npm install -g bower
 - bower install

# Running:

 - npm start
 - open browser on 127.0.0.1:8282

# Testing:

 - npm test

 (on windows: You may need to install karma)

  Results will appear when clicking the debug button at the karma server page or at http://localhost:9876/debug.html


# General:

  I've used React and Redux as the main tech stack along with bootstrap for responsive styling. Iv'e added a carousel to give the user
  more info regarding what the property has to offer. I've also added an expendable image in the heading of each room
  for the same purpose. Iv'e experimented a bit with fetch (replacing the old ajax calls) and with enzyme as a react testing helping lib.
  Iv'e also used some packages for the first time like karma-chrome-launcher (to make sure the test are run in desktop size browser) and
  react-slick for the carousel component. Hope you'll enjoy it!