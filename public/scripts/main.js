import React from 'react';
import ReactDOM from 'react-dom';

/* Redux */
import { Provider } from 'react-redux';
import configureStore from './store/configure-store.js';

/* Components */
import StudentsRoomsContainer from './containers/students-rooms-container.js';

// create the store that handles the app state
const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
      <StudentsRoomsContainer />
    </Provider>,
    document.getElementById('main')
);
