import React, { Component, PropTypes } from 'react';

/* Utilities */
import Slider from 'react-slick';

export default class HeroSection extends Component {

  render() {
    const { name, city, address, zipcode, images } = this.props;

    const sliderSettings = {
      dots: true,
      infinite: true,
      speed: 2500,
      autoplay: true,
      className: "slider",
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      centerMode: true
    };

    return (
        <section className="hero-section">
          <div className="content-container">
            <h2>{name}, {city}</h2>
            <h3>{address}, {zipcode}</h3>
          </div>
          <Slider {...sliderSettings}>
            {
              images && images.map((imageSrc, index)=> {
                return <img key={`property-image-${index}`} src={imageSrc} />
              })
            }
          </Slider>
        </section>
    )
  }
}

HeroSection.propTypes = {
  name: PropTypes.string,
  city: PropTypes.string,
  address: PropTypes.string,
  zipcode: PropTypes.string,
  images: PropTypes.array
};

HeroSection.defaultProps = {
  name: '',
  city: '',
  address: '',
  zipcode: '',
  images: []
};

