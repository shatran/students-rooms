import React, { Component, PropTypes } from 'react';

/* Utilities */
import classNames from 'classnames';
import MediaQuery from 'react-responsive';

/* Sub components */
import RoomInfo from './room-info';
import DropdownList from 'react-widgets/lib/DropdownList';

export default class RoomTypesSection extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentRoom: null,
      roomVisible: true
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.currentRoom && nextProps.roomsData && nextProps.roomsData.length) {
      this.setState({currentRoom: nextProps.roomsData[0]})
    }
  }

  onRoomOptionClick(room) {
    setTimeout(()=>{
      // switching the current room and make it visible with fade animation
      this.setState({
        currentRoom: room,
        roomVisible: true
      });
    },0);
    this.setState({roomVisible: false});
  }

  renderExpandedRoomPicker(roomsArr) {
    return (
      <ul className="room-picker">
        {
          roomsArr.map((room, index)=> {
            return (
                <li className={classNames("room-option", {"current": this.state.currentRoom === room})} key={`room-${index}`}>
                  <a onClick={this.onRoomOptionClick.bind(this, room)}>{room.name}</a>
                </li>
            )
          })
        }
      </ul>
    );
  }

  renderCollapsedRoomPicker(roomsData) {
    var self = this;
    return (
        <DropdownList
          data={roomsData}
          value={this.state.currentRoom}
          textField="name"
          onChange={(room)=> self.onRoomOptionClick.call(self, room)}
        />
    )
  }

  render() {
    const { friendsData, roomsData } = this.props;

    return (
        <div className="room-types-section">
          <h2 className="text-left">Room Types</h2>
          <div className="row">
            <div className="col-xs-12 col-md-3">
              <MediaQuery minWidth={992}>
                {
                  this.renderExpandedRoomPicker.call(this, roomsData)
                }
              </MediaQuery>
              <MediaQuery maxWidth={992}>
                {
                  this.renderCollapsedRoomPicker.call(this, roomsData)
                }
              </MediaQuery>
            </div>
            <div className="col-xs-12 col-md-9">
              <RoomInfo friendsData={friendsData} roomData={this.state.currentRoom} visible={this.state.roomVisible} />
            </div>
          </div>
        </div>
    )
  }
}

RoomTypesSection.propTypes = {
  roomsData: PropTypes.array,
  friendsData: PropTypes.object
};

RoomTypesSection.defaultProps = {
  roomsData: [],
  friendsData: {}
};
