import React, { Component, PropTypes } from 'react';

export default class BasicPropertyInfoSection extends Component {

  render() {
    const {
        numOfBeds,
        rating: {numOfVoters, average}
      } = this.props;

    return (
        <div className="row basic-info-section">
          <div className="col-xs-12 col-md-4 col-md-push-8 beds-rating-wrapper">
            <div>
              <i className="fa fa-star" aria-hidden="true"></i>
              <span>Rated average of {average} by {numOfVoters} students</span>
            </div>
            <div>
              <i className="fa fa-bed" aria-hidden="true"></i>
              <span>{numOfBeds} Total beds</span>
            </div>
          </div>
          <div className="info-wrapper col-xs-12 col-md-8 col-md-pull-4">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tortor lectus, laoreet ut congue et,
            facilisis ut risus. Curabitur ligula nisi, venenatis vel pretium sit amet, tempor ac augue.
            Maecenas euismod accumsan nulla id viverra. Aenean et euismod leo. Nam efficitur lacus in elit cursus cursus.
            Aenean hendrerit dolor quis felis luctus aliquam. Etiam risus odio, pellentesque sed enim vel,
            pharetra vestibulum lectus. Sed ut lobortis felis. Etiam volutpat iaculis risus,
            sit amet scelerisque nulla fringilla sed. Vestibulum bibendum quis erat at sollicitudin.
            In hac habitasse platea dictumst. Nunc a augue dictum, dictum ante ut, lacinia ipsum.
            Sed convallis sit amet nisi et consectetur. Morbi quam lorem, feugiat at posuere vel, consequat ut sapien.
            Quisque imperdiet, erat sed dictum bibendum, urna felis accumsan massa, et volutpat nulla eros at ex.
            Ut ac odio sed mi viverra euismod. Etiam consequat tincidunt efficitur. Integer vel mollis tortor.
            Curabitur et purus vestibulum, sodales quam sit amet, commodo justo. Integer arcu libero, tempor nec finibus ac,
            tristique id diam. Nulla ac diam vel felis sollicitudin fermentum et ultrices lorem. Praesent rhoncus mi lectus,
            ut dapibus neque viverra sit amet.
          </div>
          <div className="col-xs-12 col-md-4 col-md-push-8">
            <a className="btn btn-lg btn-block btn-primary" href="/contact_us">Contact an expert</a>
          </div>
        </div>
    )
  }
}

BasicPropertyInfoSection.propTypes = {
  numOfBeds: PropTypes.number,
  rating: PropTypes.object
};

BasicPropertyInfoSection.defaultProps = {
  numOfBeds: 0,
  rating: {
    numOfVoters: 0,
    average: 0
  }
};
