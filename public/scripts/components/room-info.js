import React, { Component, PropTypes } from 'react';

/* Sub components */
import Lightbox from 'react-images';

/* Utilities */
import Slider from 'react-slick';
import classNames from 'classnames';

let defaultRoomImage = 'https://www.student.com/bundles/oslfrontendcommon/images/sworld.png';

export default class RoomInfo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      lightboxIsOpen: false
    }
  }

  getFriendsString(roomData, friendsData) {
    if (roomData && roomData.name && friendsData && friendsData[roomData.name]) {
      let friendsArr = friendsData[roomData.name].friends;
      switch (friendsArr.length) {
        case 0:
          return null;
        case 1:
          return `${friendsArr[0]} has stayed here`;
        case 2:
          return `${friendsArr[0]} and ${friendsArr[1]} have stayed here`;
        case 3:
          return `${friendsArr[0]} and ${friendsArr[1]} and 1 other friend have stayed here`;
        default:
          return `${friendsArr[0]} and ${friendsArr[1]} and ${friendsArr.length - 2} other friend have stayed here`;
      }
    }
  }

  closeLightbox() {
    this.setState({lightboxIsOpen: false});
  }

  openLightbox() {
    this.setState({lightboxIsOpen: true});
  }

  render() {
    const { roomData, friendsData, visible } = this.props;

    let friendsString = this.getFriendsString(roomData, friendsData);
    return (
        <div className={classNames("room-info", {"visible-anim":visible})}>
          <div className="room-image-wrapper" onClick={this.openLightbox.bind(this)}>
            <img src={roomData && roomData.image || defaultRoomImage} />
            <div className="cover"></div>
          </div>
          <Lightbox
              images={[{ src: (roomData && roomData.image) || defaultRoomImage }]}
              isOpen={this.state.lightboxIsOpen}
              onClose={this.closeLightbox.bind(this)}
              onClickPrev={()=>{}}
              onClickNext={()=>{}}
          />
          <span className="room-heading">
            <h2>{roomData && roomData.name && roomData.name[0].toUpperCase() + roomData.name.slice(1) || ''}</h2>
            <h3>{roomData && roomData.description || ''}</h3>
          </span>
          <p className="room-content">{roomData && roomData.info || ''}</p>
          {
            friendsString && (
                <div>
                  <i className="fa fa-share-alt" aria-hidden="true"></i>
                  <span className="friends-visited">{friendsString}</span>
                </div>
            )
          }
          {
            roomData && roomData.facilities && roomData.facilities.length && (
                <div className="row">
                  <h2 className="col-xs-12">Facilities</h2>
                  <ul className="facilities-list">
                    {
                      roomData.facilities.map((facility, index)=> {
                        return (
                            <li className="facility col-xs-6 col-sm-3" key={`facility-${index}`}>
                              <i className="fa fa-check-circle-o" aria-hidden="true"></i>
                              <span>{facility}</span>
                            </li>
                        )
                      })
                    }
                  </ul>
                </div>
            )
          }
        </div>
    )
  }
}

RoomInfo.propTypes = {
  roomData: PropTypes.object,
  friendsData: PropTypes.object
};

RoomInfo.defaultProps = {
  roomData: {},
  friendsData: {}
};


