import React, { Component, PropTypes } from 'react';

/* Utilities */
import MediaQuery from 'react-responsive';

export default class BreadcrumbsHeader extends Component {

  render() {

    return (
      <div className="breadcrumbs-header">
        <ul className="breadcrumbs">
          <li><a href="/home">Home</a></li>
          <MediaQuery minWidth={992}>
            <li><a href="/accommodation">Accommodation</a></li>
          </MediaQuery>
          <li><a href="accommodation/london">London</a></li>
          <li><a href="" className="current">My Property</a></li>
        </ul>
      </div>
    )
  }
}
