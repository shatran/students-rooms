/* Constants */
import actionTypes from '../constants/action-types.js';


function propertyData(state = {}, action = null) {

  switch (action.type) {
    case actionTypes.RECEIVE_PROPERTY_DATA:
      return Object.assign({}, state,  {
        name: action.propertyData.name,
        city: action.propertyData.city,
        address: action.propertyData.address,
        zipcode: action.propertyData.zipcode,
        numOfBeds: action.propertyData.numOfBeds,
        rating: action.propertyData.rating,
        images: action.propertyData.images
      });

    default:
      return state;
  }
}

export default propertyData;
