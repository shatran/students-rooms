/* Constants */
import actionTypes from '../constants/action-types.js';


function friendsData(state = null, action = null) {

  switch (action.type) {
    case actionTypes.RECEIVE_FRIENDS_DATA:
      return action.friendsData;

    default:
      return state;
  }
}

export default friendsData;
