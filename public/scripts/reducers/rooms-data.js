/* Constants */
import actionTypes from '../constants/action-types.js';


function roomsData(state = [], action = null) {

  switch (action.type) {
    case actionTypes.RECEIVE_PROPERTY_DATA:
      return action.propertyData.rooms;

    default:
      return state;
  }
}

export default roomsData;

