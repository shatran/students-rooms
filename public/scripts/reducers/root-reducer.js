/* Redux */
import { combineReducers } from 'redux';
import friendsData from './friends-data';
import propertyData from './property-data';
import roomsData from './rooms-data';

export default combineReducers({friendsData, propertyData, roomsData});
