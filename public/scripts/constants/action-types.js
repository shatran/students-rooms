const REQUEST_FRIENDS_DATA = 'REQUEST_FRIENDS_DATA';
const RECEIVE_FRIENDS_DATA = 'RECEIVE_FRIENDS_DATA';
const RECEIVE_FRIENDS_DATA_ERROR = 'RECEIVE_RECEIVE_FRIENDS_DATA';

const REQUEST_PROPERTY_DATA = 'REQUEST_PROPERTY_DATA';
const RECEIVE_PROPERTY_DATA = 'RECEIVE_PROPERTY_DATA';
const RECEIVE_PROPERTY_DATA_ERROR = 'RECEIVE_PROPERTY_DATA_ERROR';

export default {
    REQUEST_FRIENDS_DATA,
    RECEIVE_FRIENDS_DATA,
    RECEIVE_FRIENDS_DATA_ERROR,
    REQUEST_PROPERTY_DATA,
    RECEIVE_PROPERTY_DATA,
    RECEIVE_PROPERTY_DATA_ERROR
}
