/* Utils */
import webRequestsAPI from '../utils/web-requests-utils';

/* Constants */
import actionTypes from '../constants/action-types';


function requestFriendsData() {
  return {
    type: actionTypes.REQUEST_FRIENDS_DATA
  }
}

function receiveFriendsData(friendsData) {
  return {
    type: actionTypes.RECEIVE_FRIENDS_DATA,
    friendsData
  }
}

function receiveFriendsDataError(error) {
  return {
    type: actionTypes.RECEIVE_FRIENDS_DATA_ERROR,
    error
  }
}

export function getFriendsData() {
  return function (dispatch) {
    dispatch(requestFriendsData());
    return webRequestsAPI.getJson('friends.json')
        .then(function (friendsData) {
          dispatch(receiveFriendsData(friendsData));
        })
        .catch(function (error) {
          dispatch(receiveFriendsDataError(error));
        })
  }
}