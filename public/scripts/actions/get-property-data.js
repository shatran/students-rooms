/* Utils */
import webRequestsAPI from '../utils/web-requests-utils';

/* Constants */
import actionTypes from '../constants/action-types';


function requestPropertyData() {
  return {
    type: actionTypes.REQUEST_PROPERTY_DATA
  }
}

function receivePropertyData(propertyData) {
  return {
    type: actionTypes.RECEIVE_PROPERTY_DATA,
    propertyData
  }
}

function receivePropertyDataError(error) {
  return {
    type: actionTypes.RECEIVE_PROPERTY_DATA_ERROR,
    error
  }
}

export function getPropertyData() {
  return function (dispatch) {
    dispatch(requestPropertyData());
    return webRequestsAPI.getJson('property-info.json')
        .then(function (propertyData) {
          dispatch(receivePropertyData(propertyData));
        })
        .catch(function (error) {
          dispatch(receivePropertyDataError(error));
        })
  }
}
