import React, { Component, PropTypes } from 'react';

/* Sub Components */
import BreadcrumbsHeader from '../components/breadcrumbs-header';
import HeroSection from '../components/hero-section';
import BasicPropertyInfoSection from '../components/basic-property-info-section';
import RoomTypesSection from '../components/room-types-section';

/* Redux */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getFriendsData } from '../actions/get-friends-data';
import { getPropertyData } from '../actions/get-property-data';


class StudentsRoomsContainer extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    // fetch friends data
    this.props.getFriendsData();
    // fetch rooms data
    this.props.getPropertyData();
  }

  render() {
    const { friendsData, roomsData, propertyData: {name ,city ,address, zipcode, numOfBeds, rating, images}} = this.props;

    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <BreadcrumbsHeader />
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <HeroSection name={name}
                           city={city}
                           address={address}
                           zipcode={zipcode}
                           images={images} />
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <BasicPropertyInfoSection numOfBeds={numOfBeds}
                                        rating={rating} />
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <RoomTypesSection friendsData={friendsData}
                                roomsData={roomsData} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// Which part of the Redux global state does our component want to receive as props
function mapStateToProps(state) {
  const {
      friendsData,
      propertyData,
      roomsData
      } = state;
  return {
    friendsData,
    propertyData,
    roomsData
  };
}

// Which action creators does it want to receive by props
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getFriendsData,
    getPropertyData
  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StudentsRoomsContainer);
