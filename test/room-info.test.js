import React from 'react';
import { shallow, mount, render } from 'enzyme';
import RoomInfo from '../public/scripts/components/room-info';

describe("Room Info Section", function() {

  const friendsData = {
    "deluxe": {
      "friends": [
        "Bob Smith",
        "Jane Doe",
        "Bubba Hyde",
        "Betsy Toheavens"
      ]
    },
    "shared": {
      "friends": [
        "Bob Smith"
      ]
    }
  };

  const roomsData = [
    {
      "name": "deluxe",
      "image": "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/the-automatic-lofts/image-nv9xdn.jpeg",
      "description": "A deluxe room",
      "info": "Nullam condimentum finibus tellus. Quisque sed quam blandit, feugiat sem ac, sagittis tortor. Aenean aliquet ante ac leo tempus, quis eleifend elit tempor. Phasellus sed condimentum mauris. Ut nec ipsum at elit rhoncus scelerisque. Morbi ornare quam et velit placerat, a imperdiet elit egestas. Nullam sodales turpis gravida nibh iaculis, ultrices elementum elit commodo.",
      "facilities": [
        "Television",
        "CCTV",
        "Bla Bla",
        "WIFI",
        "Safe",
        "Microwave",
        "Yeah"
      ]
    },
    {
      "name": "shared",
      "image": "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/the-automatic-lofts/gallery/image-2/image-nuvokh.jpeg",
      "description": "Best for travelers",
      "info": "Nam laoreet placerat tellus, in ultricies metus dignissim et. Fusce vitae maximus lorem, eu dignissim sem. Duis sed blandit dolor. Nulla malesuada vulputate tellus, vitae volutpat arcu ornare quis. Maecenas varius condimentum velit, vitae suscipit sapien pellentesque non. Donec ornare tempus leo, eget laoreet tortor finibus vulputate. Nam blandit dapibus nulla vitae sollicitudin. Nulla auctor, sapien sed varius ultricies, nisl ligula auctor orci, ut tincidunt lacus neque non dui. Morbi eget lectus sit amet lorem posuere sodales eu quis tortor.",
      "facilities": [
        "Bla Bla",
        "WIFI",
        "Safe"
      ]
    }
  ];

  it("should have a room-info and visible-anim class", function() {
    const wrapper = shallow(<RoomInfo friendsData={friendsData}
                                      roomData={roomsData[1]}
                                      visible={true} />);

    expect(wrapper.find('.room-info').length).toEqual(1);
    expect(wrapper.find('.visible-anim').length).toEqual(1);
  });

  it("should render a friends string saying: 'Bob Smith has stayed here'", function() {
    const wrapper = shallow(<RoomInfo friendsData={friendsData}
                                      roomData={roomsData[1]}
                                      visible={true} />);
    expect(wrapper.find('.friends-visited').text()).toEqual("Bob Smith has stayed here");
  });

  it("should render a friends string saying: 'Bob Smith and Jane Doe and 2 other friend have stayed here'", function() {
    const wrapper = shallow(<RoomInfo friendsData={friendsData}
                                      roomData={roomsData[0]}
                                      visible={true} />);
    expect(wrapper.find('.friends-visited').text()).toEqual("Bob Smith and Jane Doe and 2 other friend have stayed here");
  });

  it("should render 7 facilities list items", function() {
    const wrapper = shallow(<RoomInfo friendsData={friendsData}
                                      roomData={roomsData[0]}
                                      visible={true} />);
    expect(wrapper.find('.facility').length).toEqual(7);
  });
});

