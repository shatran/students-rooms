import React from 'react';
import { shallow, mount, render } from 'enzyme';
import RoomTypesSection from '../public/scripts/components/room-types-section';
import RoomInfo from '../public/scripts/components/room-info';

describe("Room Types Section", function() {

  const friendsData = {
    "deluxe": {
      "friends": [
        "Bob Smith",
        "Jane Doe",
        "Bubba Hyde",
        "Betsy Toheavens"
      ]
    },
    "shared": {
      "friends": [
        "Bob Smith"
      ]
    }
  };

  const roomsData = [
    {
      "name": "deluxe",
      "image": "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/the-automatic-lofts/image-nv9xdn.jpeg",
      "description": "A deluxe room",
      "info": "Nullam condimentum finibus tellus. Quisque sed quam blandit, feugiat sem ac, sagittis tortor. Aenean aliquet ante ac leo tempus, quis eleifend elit tempor. Phasellus sed condimentum mauris. Ut nec ipsum at elit rhoncus scelerisque. Morbi ornare quam et velit placerat, a imperdiet elit egestas. Nullam sodales turpis gravida nibh iaculis, ultrices elementum elit commodo.",
      "facilities": [
        "Television",
        "CCTV",
        "Bla Bla",
        "WIFI",
        "Safe",
        "Microwave",
        "Yeah"
      ]
    },
    {
      "name": "shared",
      "image": "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/the-automatic-lofts/gallery/image-2/image-nuvokh.jpeg",
      "description": "Best for travelers",
      "info": "Nam laoreet placerat tellus, in ultricies metus dignissim et. Fusce vitae maximus lorem, eu dignissim sem. Duis sed blandit dolor. Nulla malesuada vulputate tellus, vitae volutpat arcu ornare quis. Maecenas varius condimentum velit, vitae suscipit sapien pellentesque non. Donec ornare tempus leo, eget laoreet tortor finibus vulputate. Nam blandit dapibus nulla vitae sollicitudin. Nulla auctor, sapien sed varius ultricies, nisl ligula auctor orci, ut tincidunt lacus neque non dui. Morbi eget lectus sit amet lorem posuere sodales eu quis tortor.",
      "facilities": [
        "Bla Bla",
        "WIFI",
        "Safe"
      ]
    }
  ];

const wrapper = shallow(<RoomTypesSection friendsData={friendsData}
                                          roomsData={roomsData} />);

  it("should have a `.room-types-section` class", function() {
    expect(wrapper.find('.room-types-section').length).toEqual(1);
  });

  it("should have a room picker with 2 rooms in it", function() {
    expect(wrapper.find('.room-picker').length).toEqual(1);
    expect(wrapper.find('.room-picker').children().length).toEqual(2);
  });

  it("default room should be 'deluxe' room", function() {
    wrapper.setProps({roomsData: roomsData});
    expect(wrapper.find(RoomInfo).props().roomData.name).toEqual("deluxe");
  });

  it("current room should be 'shared' room", function() {
    wrapper.setState({ currentRoom: roomsData[1] });
    expect(wrapper.find(RoomInfo).props().roomData.name).toEqual("shared");
  });
});
