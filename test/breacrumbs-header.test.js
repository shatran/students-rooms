import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Header from '../public/scripts/components/breadcrumbs-header';

describe("Breadcrumbs Header", function() {

  it("should have a `.breadcrumbs-header` class", function() {
    const wrapper = shallow(<Header />);
    expect(wrapper.hasClass('breadcrumbs-header')).toEqual(true);
  });

  it("should exists and be identical", function() {
    expect(shallow(<Header />).is('.breadcrumbs-header')).toEqual(true);
  });

  it("should render only one .current breadcrumb with the text `My Property`", function() {
    expect(shallow(<Header/>).find('.current').length).toEqual(1);
    expect(shallow(<Header/>).find('.current').text()).toEqual("My Property");
  });
});
