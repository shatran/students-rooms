import React from 'react';
import { shallow, mount, render } from 'enzyme';
import BasicPropertyInfoSection from '../public/scripts/components/basic-property-info-section';

describe("Basic Property Info Section", function() {

  const propertyInfo = {
    numOfBeds: 20,
    rating: {
      numOfVoters: 40,
      average: 5.0
    }
  };

  const wrapper = shallow(<BasicPropertyInfoSection numOfBeds={propertyInfo.numOfBeds}
                                                    rating={propertyInfo.rating} />);

  it("should have a `.basic-info-section` class", function() {
    expect(wrapper.hasClass('basic-info-section')).toEqual(true);
  });

  it("should render one anchor button with the text: 'Contact an expert'", function() {
    expect(wrapper.find('a').length).toEqual(1);
    expect(wrapper.find('.btn').length).toEqual(1);
    expect(wrapper.find('.btn').text()).toEqual('Contact an expert');
  });


  it("should have 2 icons one with class .fa-star and one with .fa-bed", function() {
    expect(wrapper.find('i').length).toEqual(2);
    expect(wrapper.find('.fa-star').length).toEqual(1);
    expect(wrapper.find('.fa-bed').length).toEqual(1);
  });

  it("should have an text saying 'Rated average of 5 by 40 students'", function() {
    expect(wrapper.find('span').first().text()).toEqual('Rated average of 5 by 40 students');
  });

  it("should have an text saying '20 Total beds'", function() {
    expect(wrapper.find('span').at(1).text()).toEqual('20 Total beds');
  });
});


