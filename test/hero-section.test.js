import React from 'react';
import { shallow, mount, render } from 'enzyme';
import HeroSection from '../public/scripts/components/hero-section';

describe("Hero Section", function() {

  const propertyInfo = {
      "name": "My Property",
        "city": "London",
        "address": "123 liverpool road",
        "zipcode": "W1W RE23",
        "numOfBeds": 200,
        "rating": {
      "numOfVoters": 100,
          "average": 4.4
    },
      "images": [
      "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/tailor-lofts/image-nv8mk4.jpeg",
      "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/tailor-lofts/gallery/image-4/image-nv4yko.jpeg",
      "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/tailor-lofts/gallery/image-6/image-nv4yko.jpeg",
      "https://www.student.com/media/cache/light_gallery_main_desktop/mstr/country/united-states/city/chicago/property/tailor-lofts/gallery/image-5/image-nv4yko.jpeg"
    ]
  };

  const wrapper = shallow(<HeroSection name={propertyInfo.name}
                                       city={propertyInfo.city}
                                       address={propertyInfo.address}
                                       zipcode={propertyInfo.zipcode}
                                       images={propertyInfo.images} />);

  it("should render one `.slider` image slider", function() {
    expect(wrapper.find('.slider').length).toEqual(1);
  });

  it("should render 4 images in the slider", function() {
    expect(wrapper.find('img').length).toEqual(4);
  });

  it("should have an h2 with the text 'My Property, London'", function() {
    expect(wrapper.find('h2').text()).toEqual('My Property, London');
  });

  it("should have an h3 with the text'123 liverpool road, W1W RE23'", function() {
    expect(wrapper.find('h3').text()).toEqual('123 liverpool road, W1W RE23');
  });
});

