module.exports = function() {
  this.loadNpmTasks("grunt-contrib-uglify");

  this.config("uglify", {
      js: {
        expand: true,
        cwd: '<%= config.build%>/',
        src: '**/*.js',
        dest: '<%= config.dist%>/'
      }
    });
};


