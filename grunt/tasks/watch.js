module.exports = function(grunt, options) {
  var self = this;
  this.loadNpmTasks("grunt-contrib-watch");

  this.config("watch", {
    livereload: {
      options: {
        livereload: grunt.option('livereload') || true
      },
      files: ['<%= config.build %>/**/*']
    },

    html: {
      files: ['**/*.html'],
      tasks: ['copy:dev']
    },

    'sass': {
      files: ["<%= config.public %>/styles/{,*/}*.{scss,sass}"],
      tasks: ["compass"]
    }
  });

  this.event.on ('watch', function(action, filepath, target) {
    self.log.writeln(target+":  "+filepath+" has "+action+"..");
  });
};



