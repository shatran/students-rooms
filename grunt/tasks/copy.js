module.exports = function() {
  this.loadNpmTasks("grunt-contrib-copy");

  // Move files during a build.
  this.config("copy", {
    dev: {
      files: [
        {src: '<%= config.public %>/index.html', dest: '<%= config.build %>/index.html'},
        {src: 'friends.json', dest: '<%= config.build %>/friends.json'},
        {src: 'property-info.json', dest: '<%= config.build %>/property-info.json'},
        {src: 'node_modules/react-widgets/dist/css/react-widgets.css', dest: '<%= config.build %>/styles/react-widgets.css'},
        {src: 'node_modules/react-widgets/dist/css/react-widgets.css', dest: '<%= config.build %>/styles/react-widgets.css'},
        {src: 'node_modules/react-widgets/dist/fonts/rw-widgets.woff', dest: '<%= config.build %>/fonts/rw-widgets.woff'},
        {src: 'node_modules/react-widgets/dist/fonts/rw-widgets.ttf', dest: '<%= config.build %>/fonts/rw-widgets.ttf'},
        {
          expand: true,
          cwd: '<%= config.public %>/images/',
          src: ['**/*.{png,jpg,jpeg,svg,ico,gif}'],
          dest: '<%= config.build %>/images/',
          filter: 'isFile'
        }
      ]
    },
    dist: {
      files: [
        {src: '<%= config.public %>/index.html', dest: '<%= config.dist %>/index.html'},
        {src: 'friends.json', dest: '<%= config.dist %>/friends.json'},
        {src: 'property-info.json', dest: '<%= config.dist %>/property-info.json'},
        {src: 'node_modules/react-widgets/dist/css/react-widgets.css', dest: '<%= config.build %>/styles/react-widgets.css'},
        {src: 'node_modules/react-widgets/dist/fonts/rw-widgets.woff', dest: '<%= config.build %>/fonts/rw-widgets.woff'},
        {src: 'node_modules/react-widgets/dist/fonts/rw-widgets.ttf', dest: '<%= config.build %>/fonts/rw-widgets.ttf'},
        {
          expand: true,
            cwd: '<%= config.public %>/images/',
            src: ['**/*.{png,jpg,svg}'],
            dest: '<%= config.dist %>/images/',
            filter: 'isFile'
        }
      ]
    }
  });
};



